import React, { Component } from 'react';
import { StyleSheet, View, Alert, Text } from 'react-native';

export default class CurrentDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //defauilt value of the date time
      date: '',
    };
  }
  componentDidMount() {
    const that = this;
    const date = new Date().getDate(); //Current Date
    const month = new Date().getMonth() + 1; //Current Month
    const year = new Date().getFullYear(); //Current Year
    const hours = new Date().getHours(); //Current Hours
    const min = new Date().getMinutes(); //Current Minutes
    const sec = new Date().getSeconds(); //Current Seconds
    that.setState({
      //Setting the value of the date time
      date:
         month + '/' + date + '/' + year + ' ' + hours + ':' + min + ':' + sec,
    });
  }
  render() {
    console.log(this.state.date)
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection:'row'
        }}>
        <Text
            style={{
            color: 'gray',
            fontSize:10, 
            fontWeight:'bold',
            }}>
            Made on:          
        </Text>

        <Text
            style={{
            color: 'gray',
            fontSize:10, 
            fontWeight:'bold'}}>          
            {this.state.date}
        </Text>

      </View>
    );
  }
}


