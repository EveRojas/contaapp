import React , {useState} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faLock} from '@fortawesome/free-solid-svg-icons';
import { faUser} from '@fortawesome/free-solid-svg-icons';
import Constants from 'expo-constants';


const Login = props => {

const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleClick = async () => {
      if (email === '' || password === ''){
        return alert('please provide email and password')
      } 

      try {
          //console.log(email,password)
         const response = await axios.post('http://157.245.65.53/server/users/login',{email,password})
         if(response.data.ok){
            AsyncStorage.setItem('token',JSON.stringify(response.data.token))
            props.changeLogStatus()
         }
      } catch (err) {
        alert('something went wrong')
      }
  } 

  return (

    <View style={styles.container}>
    <View style={styles.statusBar} />
         <View style={styles.header}>
          <Image
          style={{ width: 120, height: 120, left:7}} 
          source={require('../assets/images/logo_white.png')}
          />
         </View>

         <View style={styles.main}>          
           <Text style={{color: 'white', fontSize:20, fontWeight:'bold'}}> CONTA MAP </Text>
             <View style={styles.login}>
                 <FontAwesomeIcon icon={faUser} color={ 'white' }/>             
             
                 <TextInput style={styles.input}
                            placeholder='email'
                            placeholderTextColor ='gray'
                            onChangeText={(text) => setEmail(text)}
                            value={email}
                              />                    
            
                 <FontAwesomeIcon icon={faLock} color={ 'white' }/> 
    
                 <TextInput  style={styles.input} 
                             placeholder='password'
                             secureTextEntry={true}
                             placeholderTextColor ='gray'
                             onChangeText={(text) => setPassword(text)}
                             value={password}
                             />
                             
               </View>    
            
             <Button  title= {"Sign In"} 
                      onPress= {()=> handleClick()}
                      color= {'white'}/>
              
          </View>
         

          <View style={styles.footer}>
                 
          <Text style={{color: 'gray'}}> Don't have an account? </Text>
          <Button         title= {"Sign Up"} 
                          onPress= {()=> props.changePage('register')}
                          color= {'white'}/>
          </View>
       
      
    </View>
  );
 }

export default Login;

const styles = StyleSheet.create({

  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  
  login:{
   
   width:'30%',
   marginTop:100,
   alignItems: 'center',
   justifyContent: 'space-between',

  },

  input:{
    height: 40, 
    width:300,
    marginTop:4,
    backgroundColor: 'black',
    borderBottomColor: 'gray',
    borderWidth: 2,
    marginBottom: 30,
    color: 'white'
  },

  footer:{
    width:'100%',
    height:'10%',
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor:'black',
    borderColor:'black',
    alignItems: 'center',
    justifyContent: 'space-between',

  },    


});
