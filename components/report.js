import React , {useState} from 'react';
import axios from 'axios';
import { StyleSheet, 
       Text, 
       View, 
       TextInput,
       Button, 
       ScrollView,
       AsyncStorage,
       Modal,
       Picker,
       TouchableOpacity,
       Image } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowsAltV } from '@fortawesome/free-solid-svg-icons';
import { faCopyright } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { GoogleAutoComplete } from 'react-native-google-autocomplete';
import Place from './place';
import CurrentDate from './current_date';
import Evidence from './evidence';
import RNCloudinary from 'react-native-cloudinary';
import Constants from 'expo-constants';


class Report extends React.Component{
constructor(props){
 super(props)
 this.state ={
    complaint:'',
    bias:'',
    agressor:'',
    acting:'',
    info:'',
    Ip:'banana',
    proof:[],
    display: false,
    record_Location:{}, 
    loc_victim:{},
    //user_id:''
    error:'',
    date: new Date().toDateString(),
    }
}
componentDidMount(){
  console.log('REPORTS')
  this.setState({loc_victim:this.props.loc_victim})
   console.log(this.props) 
  this.setState({proof:this.props.cloudinary})
}

    
  handleClick = async (event) => {
    console.log(this.state)
    
    let {complaint, bias, agressor, acting, info, record_Location} = this.state
    if(complaint==='' ||bias===''||agressor===''|| acting===''|| info===''|| record_Location==={}){
      return alert('Please fill the items!')
      //complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim 
    }

    let url = `http://157.245.65.53/server/report/create`;
    try {
      let {complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim } = this.state
      const res = await axios.post(url,{user_id:this.props.user_id,complaint, bias, agressor, acting, info, proof, record_Location, Ip, loc_victim });
      console.log('res CREATE===>',res);
          alert('Your complain was submited!')
    } catch (error) {
      this.setState({error:'something went wrong'})      
    }
}

  getAddress = async (ad) => {
    
     const country = ad.terms[ad.terms.length-1].value
     console.log(country)
     this.setState({record_Location:country})
 
}

  setProof = (el) => {
      const { proof } = this.state
      proof.push(el)
      this.setState({proof:proof,display:true})
      setTimeout(()=>this.setState({display:false}),5000)
          

}


render(){

return (

   
<View style={styles.container}>
    <View style={styles.statusBar} />
    <View style={styles.header}>
          <Image
          style={{ width: 120, height: 120}} 
          source={require('../assets/images/logo_white.png')}
          />
    </View>

 <View style={styles.main}>
       <View style={styles.caps}>
       <Text style={{ color: 'white', fontSize:20, fontWeight:'bold'}}> REPORT </Text>   
       <CurrentDate CurrentDate={this.props.CurrentDate}/>
       </View>
<ScrollView>
    <View style={styles.pick}>         
       <Text style={{ color: 'white', fontSize:15, fontWeight:'bold'}}>COMPLAINT:</Text>
       <View style={{  flex:1,
                       alignItems: 'center',
                       justifyContent: 'space-around',
                       flexDirection:'row'
                    }}>
       <Text style={{ color: 'gray', fontSize:10, fontWeight:'bold'}}> scroll the button to select </Text>
         <FontAwesomeIcon icon={ faArrowsAltV } color={ 'gray' } size={15}/>
       </View>
       <Picker
          style={styles.twoPickers} itemStyle={styles.twoPickerItems}
          selectedValue={this.state.complaint}
          onValueChange={(itemValue, itemIndex) => this.setState({complaint: itemValue})}>        
          <Picker.Item label="Discrimination" value="Discrimination"/>         
          <Picker.Item label="Intimidation" value="Intimidation" />
          <Picker.Item label="Harassment" value ="Harassment"/>
          <Picker.Item label="Physical Violence" value ="Physical Violence"/>
          <Picker.Item label="Censorship" value ="Censorship"/>
          <Picker.Item label="Prohibition of Protest" value ="Prohibition of Protest"/>
          <Picker.Item label="Other Complaint" value ="Other Complaint"/>   
      </Picker>

    </View>

    <View style={styles.pick}> 
       <Text style={{ color: 'white', fontSize:15, fontWeight:'bold'}}>ON GROUNDS OF:</Text>
       <Picker
          style={styles.twoPickers} itemStyle={styles.twoPickerItems}
          selectedValue={this.state.bias}
          onValueChange={(itemValue, itemIndex) => this.setState({bias: itemValue})}>
          <Picker.Item label="Sex, Gender, Sexuality" value="Sex, Gender, Sexuality" />
          <Picker.Item label="Faith, Beliefs" value="Faith, Beliefs" />
          <Picker.Item label="Ethnicity, Race, Nationality" value ="Ethnicity, Race, Nationality"/>
          <Picker.Item label="Political Opnions, Affiliation" value ="Political Opnions, Affiliation"/>
          <Picker.Item label="Social Status" value ="Social Status"/>
          <Picker.Item label="Age,Physical Features" value ="Age,Physical Features"/>
          <Picker.Item label="Other Bias" value ="Other Bias"/>
       </Picker>
    </View>

    <View style={styles.pick}> 
       <Text style={{ color: 'white', fontSize:15, fontWeight:'bold'}}>PERPETRATED BY:</Text>
       <Picker
          style={styles.twoPickers} itemStyle={styles.twoPickerItems}
          selectedValue={this.state.agressor}
          onValueChange={(itemValue, itemIndex) => this.setState({agressor: itemValue})}>
          <Picker.Item label="Commom Citizen" value="Commom Citizen" />
          <Picker.Item label="Public Agent" value="Public Agent" />
          <Picker.Item label="Private Agent" value="Private Agent"/>
          <Picker.Item label="Other Agressor" value="Other Agressor"/>
       </Picker>
    </View>
   
    <View style={styles.pick}> 
      <Text style={{ color: 'white', fontSize:15, fontWeight:'bold'}}>ACTING:</Text>
       <Picker
          style={styles.twoPickers} itemStyle={styles.twoPickerItems}
          selectedValue={this.state.acting}
          onValueChange={(itemValue, itemIndex) => this.setState({acting: itemValue})}>
          <Picker.Item label="Alone" value="Alone" />
          <Picker.Item label="In Group" value="In Group" />
          <Picker.Item label="Other" value ="Other"/>
       </Picker>
    </View>

    <View style={styles.pick}> 
     <Text style={{ color: 'white', fontSize:15, fontWeight:'bold',  marginTop:2, marginBottom:4}}>WHAT HAPPENED:</Text>
      <TextInput 
          style={{
            height: 60,
            width: 350,
            borderWidth: 1,
            color:'white',
            backgroundColor:'black',
            borderColor:'white',
            borderRadius:5,
            paddingHorizontal: 16,    
          }}
          placeholder='Tell us some details...'
          placeholderTextColor ='gray' 
          onChangeText={text => this.setState({info: text})}
        />
    </View>

    <View style={styles.pick}> 
     <Text style={{ color: 'white', fontSize:15, fontWeight:'bold',  marginTop:4, marginBottom:4}}>WHERE HAPPENED:</Text>
      <Place getAddress = {this.getAddress}/>
    </View>
       
    <View style={styles.pick}> 
     <Text style={{ color: 'white', fontSize:15, fontWeight:'bold',  marginTop:4}}>GET EVIDENCE:</Text>
     <Evidence onPress= {()=>this.setProof()}/>
    </View>
   <View style={styles.pick}> 
     <Text style={{ color: 'white', fontSize:15, fontWeight:'bold'}}>SUBMIT:</Text> 
     <Text style={{ color: 'gray', 
                    fontSize:10, 
                    marginRight:2}}> I accept the terms of use and privacy</Text>    

     <TouchableOpacity style={{ flex: 1, 
                                  alignItems: 'center', 
                                  justifyContent: 'center', 
                                  height: 60,
                                  width: 350,
                                  borderWidth: 1,
                                  backgroundColor:'black',
                                  borderColor:'white',
                                  borderRadius:5, 
                                  marginBottom: 15, 
                                  marginTop:4,      
                                  }} 
                        onPress= {()=> this.handleClick()}>
      <FontAwesomeIcon icon={faCopyright} color={ 'darkorange' } size={30}/>
      </TouchableOpacity>        
    </View> 
      
  </ScrollView>

 </View>
</View>    

    )
  }
}
export default Report;


   //   <UploadImages className='evidence-button'  setProof={this.setProof}/>
   //            {this.state.display 
   //    ? <h5>File uploaded!</h5>
   //    : null 
   //   }deleteThistodo = async (i) => {
 //   {
   //    this.state.proof.map((el)=>  <div>
   //     <img style={{height:'5vh', width:'5%' }} src={el.photo_url} alt='cloudinary'/>
   // <FontAwesomeIcon onClick={()=>this.destroyProof(el.public_id)} icon={faTrash} size="15"/>       
   // </div>)
   //   }



const styles = StyleSheet.create({

  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },

  input:{
    height: 40, 
    width:200,
    marginTop:4,
    backgroundColor: 'black',
    borderBottomColor: 'gray',
    borderWidth: 2,
    color: 'white'
  },

  submit:{
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center', 
    height: 60,
    width: 350,
    borderWidth: 1,
    backgroundColor:'black',
    borderColor:'white',
    borderRadius:5, 
    marginBottom: 15, 
    marginTop:4,
    }, 

  navbar:{
    alignItems: 'flex-start',
    paddingVertical: 15,
    flexDirection:'row',
    width:'100%',
    height:'10%',
    borderWidth: 1,
    backgroundColor:'white',
    borderColor:'black',
    justifyContent: 'space-around',

  }, 

  pick:{
    alignItems: 'center',
    justifyContent: 'center',
  },

  twoPickers: {
    width:350,
    height:60,
    backgroundColor: 'black',
    borderColor: 'white',
    borderRadius:5,
    marginTop:4,
    marginBottom:4,
    //marginLeft:100,
    borderColor: 'white',
    borderWidth: 1,
  },
  twoPickerItems: {
    height: 60,
    color: 'white',
    fontSize: 20
  },

  submit:{
    alignItems: 'center',
    flexDirection:'row',   
  },

    caps:{
    width:380,
    height:'12%',
    marginBottom: 5,
    //borderWidth: 1,
    //borderColor: 'gray',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

});



    
        
        

    

