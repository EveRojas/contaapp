import React , {useState, useEffect} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         Alert,
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image,
         TouchableOpacity } from 'react-native';
import FontAwesome, { 
       Icons,
       IconTypes} from 'react-native-fontawesome';
//import MapSearch from './map';
import MapView from 'react-native-maps';
import Marker from 'react-native-maps';
import MapIntro from './mapintro';
import Constants from 'expo-constants';



const Home = (props) => {

// useEffect( () => {
//       props.changePage()
//           },[])


return (
<View style={styles.container}>
     <View style={styles.statusBar} />
  <View style={styles.header}>
            <Image
            style={{ width: 120, height: 120 }} 
            source={require('../assets/images/logo_white.png')}
            />   
  </View>

  <View style={styles.main}>
            <Text style=
                  {{ color: 'white',
                     fontSize:20, 
                     fontWeight:'bold', 
                     marginBottom:10 }}> CONTA MAP </Text>
          <View style={styles.call}>
             <Text style={{
            color: 'gray',  
            fontWeight:'bold',
            marginTop:8, 
            }}>Had you been a victim? </Text> 
            <Button 
             title='Report it!'
             color='darkorange'
             onPress={()=>props.changePage('report')}
            />
          </View>
 
     <MapIntro MapIntro={props.MapIntro}/>
  </View>      
  
</View>


    )
}

export default Home;

const styles = StyleSheet.create({
  
  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 0,
    marginBottom:0,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  
  mediaicons:{
   alignItems: 'center',
   justifyContent: 'space-around',
   flexDirection:'row',
  },

  call:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'space-around',
    //flexDirection:'row',
    borderTopColor:'white',
    borderBottomColor:'white',
    marginBottom:0,
    marginTop:0,
    borderWidth:1,
    //borderRadius:5,
    width:380,
    height: 20,
    
  }

});



//<View style={{ backgroundColor:'white', color: 'black', fontSize:10, height:32, width:210, borderRadius:5, marginTop:1}}>
          //</View> 


           // <TouchableOpacity>
          // <FontAwesome type={IconTypes.FAB} color={ 'white' } size={30}>
          //                    {Icons.facebookSquare }</FontAwesome>
          // </TouchableOpacity>

          // <TouchableOpacity>
          //  <FontAwesome type={IconTypes.FAB} color={ 'white' } size={30}>
          //                    {Icons.twitterSquare }</FontAwesome>
          // </TouchableOpacity>

          // <TouchableOpacity>
          //  <FontAwesome type={IconTypes.FAB} color={ 'white' } size={30}>
          //                    {Icons.chevronLeft}</FontAwesome>
          // </TouchableOpacity>