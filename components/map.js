import React , {useState} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image,
         TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Marker from 'react-native-maps';
import Constants from 'expo-constants';



class MapSearch extends React.Component { 

constructor(props){
 super(props);
 this._getCoords = this._getCoords.bind(this);  
 this.state = {
     api_key:'AIzaSyAXNguhfKzdPRxgxf9YISL9XcnhWnlwDKQ',
     address:'',
     results: [],
     markers:'',
     markerIcons:{},
     initialPosition:{
      latitude:0,
      longitude: 0,
     },
      }
   };


componentDidMount(){

    this._getCoords();

    fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=bucharest&sensor=false&key=${this.state.api_key}`)//&type=restaurant
    .then( res => res.json() )
    .then( resJson => {console.log(resJson);this.setState({results:resJson.results}) })
    .catch( err => console.log(err) )
}

onChangeText = (text) => {
   //console.log(text)
 this.setState({address:text})
}



_getCoords = () => {
    navigator.geolocation.getCurrentPosition((position) => {
        var lat = parseFloat(position.coords.latitude)
        var long = parseFloat(position.coords.longitude)
       
        var initialRegion ={
          latidude:lat,
          longitude:long,
             }

          this.setState({initialPosition:initialRegion})
         },
         (error)=>alert(JSON.stringify(error)),
         {enableHightAccuracy:true, timeout:100, maximunAge:1000}
        );

}

render(){
  
//    console.log(this.props.stats)
return (

   
<View style={styles.container}>
    <View style={styles.statusBar} />
    <View style={styles.header}>
          <Image
          style={{ width: 120, height: 120}} 
          source={require('../assets/images/logo_white.png')}
          />
          
    </View>

    <View style={styles.main}>
       
        <Text style={{ color: 'white',
                       fontSize:20, 
                       fontWeight:'bold', 
                       marginBottom:8}}> MAP OF INTOLERANCE </Text>
         <View style={styles.search}>    
             <Text style={{color:'gray', marginTop:5}}>Would you like a kinder world?</Text>
             <Button
                   title='Report!'
                   color='darkorange'
                   onPress={()=>props.changePage('report')}/>
          </View> 

        <MapView
              style={styles.mapdisplay}
              provider={PROVIDER_GOOGLE}
              region={this.state.initialPosition}
              showsUserLocation={true}
              followUserLocation={true}
              fetchDetails={true}
              region={this.state.region}
              onRegionChange={this.onRegionChange}
            >
              {this.props.stats.map((ele, index) => {
                console.log('ele ===>',ele)
               return <MapView.Marker//return in the same line of return
                        key={index}
                        //image={require('../assets/images/pinpoint.png')}   
                        coordinate={{
                               latitude: ele.latd,
                               longitude: ele.long,
                               }}
                        onPress={()=>this.props.getNumbers(ele)}
                               >
              <View style={styles.caption}> 
              <Text style={{color: 'darkorange', fontWeight:'bold', fontSize:18}}>
                {ele.count}
              </Text>
              <Image source={require('../assets/images/pinpoint.png')} style={{height:30, width:20}}/>
                </View>  
                  </MapView.Marker>
               
              })}


        </MapView>
                            
         
    </View>
  

</View>

    )
  }
}
export default MapSearch;






const styles = StyleSheet.create({

  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
    
  
  mapdisplay:{
    height:487, 
    width:380,
    marginTop:0,
    marginBottom:0,
    borderTopColor:'gray',
    borderWidth:1,
    borderBottomColor:'gray',
  },

  search:{
    alignItems: 'center',
    justifyContent: 'space-around',
    //flexDirection:'row',
    backgroundColor:'black',
    borderTopColor:'white',
    borderBottomColor:'white',
    //borderColor:'white',
    borderWidth:1,
    //borderRadius:5,
    width:380,
    height: 60,
    marginTop:5   
  },


  icons:{
   alignItems: 'center',
   justifyContent: 'space-between',
   flexDirection:'column',
   paddingTop:28

  },

  caption:{
    alignItems: 'center',
    justifyContent: 'center',
  },

  call:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'space-around',
    //flexDirection:'row',
    borderTopColor:'white',
    borderBottomColor:'white',
    marginBottom:0,
    marginTop:0,
    borderWidth:1,
    //borderRadius:5,
    width:380,
    height: 70,
    
  }

});


           // <View style={styles.search}>    
          //    <TextInput  style={{ color: 'white', fontSize:15}}   
          //     onChangeText={text => this.onChangeText(text)} 
          //     placeholder={'Search by country...'}  
          //     placeholderTextColor ='gray'
          //     value={this.state.key_word}/>

          //       <TouchableOpacity  color='darkorange'>   
          //       <FontAwesomeIcon icon={faSearch} color={ 'white' } size={20}/> 
          //     </TouchableOpacity>
          // </View> 
