import React , {useState} from 'react';
import axios from 'axios';
import { StyleSheet, 
       Text, 
       View, 
       TextInput,
       Button, 
       ScrollView,
       AsyncStorage,
       Modal,
       Picker,
       Image } from 'react-native';
// apiKey="AIzaSyAXNguhfKzdPRxgxf9YISL9XcnhWnlwDKQ"
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

const Place = (props) => {
  return (
    <GooglePlacesAutocomplete
      placeholder='Address...'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
      listViewDisplayed='false'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
           props.getAddress(data);
      }}

      getDefaultValue={() => ''}

      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyAXNguhfKzdPRxgxf9YISL9XcnhWnlwDKQ',
        language: 'en', // language of the results
        types:'geocode' // default: 'geocode'
      }}

      styles={{
        textInputContainer: {          
            height: 60,
            width: 350,
            borderWidth: 1,
            backgroundColor:'transparent',
            borderColor:'white',
            borderRadius:5,
            alignItems: 'center',
            justifyContent: 'center',
            //marginLeft: 12,    
          //width: '100%'
         // backgroundColor: 'white'
        },
        description: {
          fontWeight: 'bold',
          color:'white'
        },
        predefinedPlacesDescription: {
          color: 'white'
        }
      }}
      


      currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      //GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        //rankby: 'distance',
        //type: 'cafe'
      //}}
      
      GooglePlacesDetailsQuery={{
        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
        fields: 'formatted_address',
      }}

     // filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      //predefinedPlaces={[homePlace, workPlace]}

      //debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
     // renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
      //renderRightButton={() => <Text>Custom text after the input</Text>}
    />
  );
}
  export default Place;