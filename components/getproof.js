import React , {useState, useEffect} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         Alert,
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image,
         TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCamera} from '@fortawesome/free-solid-svg-icons';
import { faVideo } from '@fortawesome/free-solid-svg-icons';
import Evidence from './evidence'
//import Login from './components/login';
//import Report from './components/report';
//import MapSearch from './components/map';


const Proof = (props) => {


return (
<View style={styles.container}>
 
    <View style={styles.header}>
            <Image
            style={{ width: 120, height: 120, }} 
            source={require('../assets/images/logo_white.png')}
            />   
    </View>

    <View style={styles.main}>
      <Text style=
            {{color: 'white',  
              fontSize:20, 
              fontWeight:'bold',
              marginBottom:60}}> GET EVIDENCE </Text>  
     <ScrollView>       
      <View style={styles.proof}>
		    <View style={styles.pick}> 
		     <Evidence/>
		    </View>
		 
	          <View style={styles.fileicons}>
	          <TouchableOpacity>
	          <FontAwesomeIcon icon={ faCamera } color={ 'white' } size={30}/>                     
	          </TouchableOpacity>

	          <TouchableOpacity>
	          <FontAwesomeIcon icon={ faVideo } color={ 'white' } size={30}/>
	          </TouchableOpacity>
	          </View>
	           
            
           <Button 
             title='Save'
             color='white'
             //onPress={()=>props.changePage('report')}
            />
           <Button 
             title='Skip'
             color='white'
             onPress={()=>props.changePage('report')}
            />
             
      </View>
     </ScrollView>
    </View>


</View>


    )
}

export default Proof;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  
  pick:{
    alignItems: 'center',
    justifyContent: 'center',
  },

  proof:{
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection:'row',
    backgroundColor:'black',
    borderColor:'white',
    borderWidth:1,
    borderRadius:5,
    width:370,
    height: 50,
    
  },

  fileicons:{
   alignItems: 'center',
   justifyContent: 'space-around',
   flexDirection:'row',
  },

});

