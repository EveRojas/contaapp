import React , {useState} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faLock} from '@fortawesome/free-solid-svg-icons';
import { faUser} from '@fortawesome/free-solid-svg-icons';
import Constants from 'expo-constants';


const Register = props => {

const[values,setValues]= useState({
     
     email:'',
     password:'',
     password2:'',
     hidden:true
})

  const [ message , setMessage ] = useState('')
  const onChangeText = e => {
       setValues({...values,[e.target.name]:e.target.value})
  }
  const handleClick = async (e) => {

    if (values.email===''|| values.password===''|| values.password2===''){
         return alert('please fill the items')
       }

    try{
      const response = await axios.post('http://157.245.65.53/server/users/register',{
          email    : values.email,
          password : values.password,
          password2: values.password2
          }) 
      
      setMessage(response.data.message)
        console.log('MES RESP==>',response)
        props.changePage('login')
        
      }
    catch( error ){
      alert('something went wrong')
  
    }
  
}

  return (

    <View style={styles.container}>
            <View style={styles.statusBar} />
         <View style={styles.header}>
          <Image
          style={{ width: 120, height: 120, left:7}} 
          source={require('../assets/images/logo_white.png')}
          />
         </View>

         <View style={styles.main}>          
         <Text style={{color: 'white', fontSize:20, fontWeight:'bold' }}>REGISTER</Text>
             <View style={styles.login}>
                 <FontAwesomeIcon icon={faUser} color={ 'white' }/>             
             
                 <TextInput style={styles.input}
                            placeholder='insert email'
                            placeholderTextColor ='gray'
                            onChangeText={(text) => setValues({...values,email:text})}
                            value={values.email}
                              />                    
            
                 <FontAwesomeIcon icon={faLock} color={ 'white' }/> 
    
                 <TextInput  style={styles.input}
                             placeholder='insert password'
                             secureTextEntry={true}
                             placeholderTextColor ='gray' 
                             onChangeText={(text) => setValues({...values,password:text})}
                             value={values.password}
                             />

                 <FontAwesomeIcon icon={faLock} color={ 'white' }/> 
    
                 <TextInput  style={styles.input} 
                             placeholder='repeat password'
                             secureTextEntry={true}
                             placeholderTextColor ='gray'
                             onChangeText={(text) => setValues({...values,password2:text})}
                             value={values.password2}
                             />
                             
               </View>    
            
             <Button  title= {"Sign Up"} 
                      onPress= {()=> handleClick()}
                      color= {'white'}/>
              
          </View>
         

          <View style={styles.footer}>
          
          <Text style={{color: 'gray'}}> Already an user? </Text>
          <Button         title= {"Sign In"} 
                          onPress= {()=> props.changePage('login')}
                          color= {'white'}/>
           
          </View>
       
      
    </View>
  );
 }

export default Register;

const styles = StyleSheet.create({

  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  
  login:{
   
   width:'30%',
   marginTop:70,
   alignItems: 'center',
   justifyContent: 'space-between',

  },


  input:{
    height: 40, 
    width:300,
    marginTop:4,
    backgroundColor: 'black',
    borderBottomColor: 'gray',
    borderWidth: 2,
    marginBottom: 30,
    color: 'white'
  },

  footer:{
    width:'100%',
    height:'10%',
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor:'black',
    borderColor:'black',
    alignItems: 'center',
    justifyContent: 'space-between',

  },    


});
