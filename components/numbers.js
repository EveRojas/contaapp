import React , {useState, useEffect} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image,
         TouchableOpacity } from 'react-native';
import Report from './report';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChartBar } from '@fortawesome/free-solid-svg-icons';
import { faChartPie } from '@fortawesome/free-solid-svg-icons';
import Constants from 'expo-constants';


class Numbers extends React.Component {
 
 state={
  complaint_array : [],
  bias_array:[],
  name_array:[],
 }

 componentDidMount(){
  
   if( this.props.stats.length > 0){
      const { _id } = this.props.numbers
      const index = this.props.stats.findIndex( ele => ele._id === _id)
      const complaint_array = Object.entries(this.props.stats[index].complaint)
         
      this.setState({complaint_array})
    console.log('TYPE OF COMP===>', complaint_array)
   }

    if( this.props.stats.length > 0){
      // console.log('props=>',this.props.stats)
      const { _id } = this.props.numbers
      //console.log(this.props.match.params)
      const index = this.props.stats.findIndex( ele => ele._id === _id)
      
      const bias_array = Object.entries(this.props.stats[index].bias)    
      this.setState({bias_array})
      //console.log('BIAS===>', bias_array)
   }  

     if( this.props.stats.length > 0){
      // console.log('props=>',this.props.stats)
      const { _id } = this.props.numbers
      //console.log(this.props.match.params)
       const index = this.props.stats.findIndex( ele => ele._id === _id)
       this.setState({name_array:this.props.stats[index].name})

      }

    }
 
render(){
  console.log('===  TYPE COMP  ==>',this.props)
 return(<View style={styles.container}>
      <View style={styles.statusBar} />
              <View style={styles.header}>
                      <Image
                      style={{ width: 120, height: 120, left:7}} 
                      source={require('../assets/images/logo_white.png')}
                      />
                </View>
                    
                <View style={styles.main}>
                <Text style=
                     {{color: 'white',
                       fontSize:20,
                       fontWeight:'bold', 
                       marginBottom:30}}>RECORDS</Text>  
           
                <View>
                 <Text style=
                       {{color: 'darkorange',
                         fontSize:20, 
                         fontWeight:'bold'}}>{this.state.name_array} </Text>
                </View>
            <ScrollView>     
                <View style={styles.navicon}>
                 <FontAwesomeIcon icon={faChartBar} 
                                  color={ 'white' } 
                                  size={80}/> 
                       {
                        this.state.complaint_array.map( (ele,idx) => {
                           return <Text style= {{
                                        color: 'white',
                                        fontSize:20}} 
                                        key={idx}>{ele[0]}.... {ele[1]}</Text>
                        })
                      } 
                </View>
                
                <View style={styles.navicon}>
                 <FontAwesomeIcon style= {{
                                  marginBottom:10}} 
                                  icon={faChartPie} 
                                  color={ 'white' } 
                                  size={80}
                                   />
                 {
                  this.state.bias_array.map( (ele,i) => {
                       return <Text style= {{
                                    color: 'white',
                                    fontSize:20
                                    }} 
                                    key={i}>{ele[0]} ....{ele[1]}</Text> 
                   })
                 }
                 </View>
            </ScrollView>

              </View>
       
        </View>
    )
  } 
}
export default Numbers; 

const styles = StyleSheet.create({

  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },

  navicon:{
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginTop:30,
    marginBottom:20

  },


});