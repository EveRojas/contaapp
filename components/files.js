import React , {useState, useEffect} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         Alert,
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image,
         TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMinusSquare} from '@fortawesome/free-solid-svg-icons';
import { faCaretSquareDown } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import Constants from 'expo-constants';



const Files = (props) => {
   
   
   useEffect( () => {
         props.getUserReports()
   })
 

return (
<View style={styles.container}>
     <View style={styles.statusBar} />
    <View style={styles.header}>
            <Image
            style={{ width: 120, height: 120 }} 
            source={require('../assets/images/logo_white.png')}
            />   
    </View>

    <View style={styles.main}>
      <Text style=
            {{color: 'white',  
              fontSize:20, 
              fontWeight:'bold',
              marginBottom:60}}> MY REPORTS </Text>  
     <ScrollView>       
         {
          props.user_reports.length > 0 
          ? props.user_reports.map((ele,i) => {
  
          return <View key={i} style={styles.records}>
                       <Text style= {{color: 'white',  
                                      fontSize:15, 
                                      fontWeight:'bold'}}>{ele.createdAt}</Text>
                                        <View style={styles.fileicons}>
                      <TouchableOpacity onPress= {()=>props.changePage('show_report',ele)}>
                      <FontAwesomeIcon icon={ faCaretSquareDown } color={ 'white' } size={30}/>                     
                      </TouchableOpacity>

                      <TouchableOpacity onPress= {()=>props.changePage('editreport',ele)}>
                      <FontAwesomeIcon icon={ faPenSquare } color={ 'white' } size={30}/>
                      </TouchableOpacity>

                      <TouchableOpacity
                              title ={'open alert'} 
                                   onPress={()=>Alert.alert(
                                   'Delete',

                                   'Are you sure?',
                                  [
                                  {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                  {text: 'OK', onPress: () => props.deleteUserReport(i)},
                                  ],
                                  { cancelable: false }

                            )}

                      >
                      <FontAwesomeIcon icon={ faMinusSquare } color={ 'white' } size={30}/>
                      </TouchableOpacity>
                      </View>

                 </View>
           })
           : null
        }
     </ScrollView>
    </View>
</View>


    )
}

export default Files;

const styles = StyleSheet.create({
  statusBar: {
    height: Constants.statusBarHeight,
  },
  
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  

  input:{
    height: 40, 
    width:200,
    marginTop:4,
    backgroundColor: 'black',
    borderBottomColor: 'gray',
    borderWidth: 2,
    color: 'white'
  },


  records:{
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection:'row',
    backgroundColor:'black',
    borderColor:'white',
    color:'white',
    borderWidth:1,
    borderRadius:5,
    width:370,
    height: 50,
    
  },

  fileicons:{
   alignItems: 'center',
   justifyContent: 'space-around',
   flexDirection:'row',
  },

// shadow:{
//   borderWidth: 1;
//   borderRadius: 2;
//   borderColor: #ddd;
//   bordeBottom: 0;
//   shadowColor: #000;
//   shadowOffset: {width: 0, height: 2};
//   shadowOpacity: 0.8;
//   shadowRadius: 2;
//   elevation: 1;
//   marginLeft: 5;
//   marginRight: 5;
//   marginTop: 10;
// }
});


// <TextInput style={{
//                      color: 'white',  
//                      fontSize:20, 
//                      }}
//                      placeholder="My report: date"
//                      placeholderTextColor ='gray'
//                      />
          

// state={
//     user_report:'',
//     user_reports:[],
//     modalVisible: false,
//     index: undefined 
//   }




//////////////////// async function ///////////////////


// const update = async(req,res) => {

// try{
     
//   const response = await axios.post(
//   'http://157.245.65.53/server/report/user_reports',{token:JSON.parse(token)})
// if(response.data.ok){
//             AsyncStorage.setItem('token',JSON.stringify(response.data.token))
//          }
//       } catch (err) {
//         alert('something went wrong')
//       }
//   }

// handleUpdate = async()=>{

// try{

//   const {index, user_reports, user_report} = this.state
//   user_reports[index]=user_report
//   await AsyncStorage.setItem('user_reports', JSON.stringify(user_reports));
//   this.setState({user_report:'', user_reports, index:undefined, modalVisible:false})


// } catch(error){

//   console.log(error.message)
  
//   }

// }


//////////////////////////////////////////
