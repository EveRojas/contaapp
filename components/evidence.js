import * as React from 'react';
import { Button, Image, View, Modal, } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import RNCloudinary from 'react-native-cloudinary';

export default class Evidence extends React.Component {
  state = {
    image: null,
    video:null,
    index: undefined,
    modalVisible:false 
  };

  openModal =(image)=> this.setState({modalVisible:true, index:image})


  componentDidMount() {
    this.getPermissionAsync();
    console.log('hi');
    this.getPermissionCameraAsync();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  getPermissionCameraAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      takePhoto: true,
      cancelled: true,
      aspect: [4, 3],
      quality: 1
    });
    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri, modalVisible:true });

    let base64Img = `data:image/jpg;base64,${result.base64}`
      
      let apiUrl = 'https://api.cloudinary.com/v1_1/dyfnrqoys/image/upload';
  
      let data = {
        "file": base64Img,
        "upload_preset": "contamap",
      }

      fetch(apiUrl, {
        body: JSON.stringify(data),
        headers: {
          'content-type': 'application/json'
        },
        method: 'POST',
      }).then(async r => {
          let data = await r.json()
          console.log(data.secure_url)
          return data.secure_url
      }).catch(err=>console.log(err))


    }
  };




  render() {
    let { image } = this.state;
    //let { video } = this.state;

   
    return (
      <View style={{ 
          flex: 1, 
          alignItems: 'center', 
          justifyContent: 'center', 
          height: 60,
          width: 350,
          borderWidth: 1,
          backgroundColor:'black',
          borderColor:'white',
          borderRadius:5, 
          marginBottom: 15, 
          marginTop:4,      
          }}>

           <Button
          title="Pick from your camera roll"
          color='white'
          onPress={()=>this.openModal()}
           />
       
       <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          
         
        {image &&
          <Image source={{ uri: image }} style={{ width: 200, 
                                                  height: 200, 
                                                  marginTop:30 }} />}
            <View style={{flex:1,  
                  alignItems:'center', 
                  justifyContent:'center' 
                  }}> 

                 <Button   
                           title="Select proof"
                           color='black'
                           onPress={this._pickImage}
                            />

                 <Button   title={'Save & Close'}
                           onPress={()=>this.setState({image:'',  
                                                       modalVisible:false})}
                           color= {'darkorange'}
                           />    
            </View>
       </Modal>
      </View>
    );
  }
}

// _pickVideo = async () => {
//     let result = await ImagePicker.launchCameraAsync({
//       mediaTypes: ImagePicker.MediaTypeOptions.All,
//       allowsEditing: true,
//       takePhoto: true,
//       cancelled: true,
//       aspect: [4, 3],
//       quality: 1
//     });

//     console.log(result);

//     if (!result.cancelled) {
//       this.setState({ video: result.uri });
//     }
//   };
// }
// const styles = StyleSheet.create({

//   model: {
//     flex: 1,
//     backgroundColor: "black",
//     alignItems: 'center',
//     justifyContent: 'center',
//   }

// });

