import React , {useState, useEffect} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View,
         Share, 
         TextInput,
         Button, 
         Alert,
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image,
         TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMinusSquare} from '@fortawesome/free-solid-svg-icons';
import { faShareAltSquare } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import Files from './files';
import Constants from 'expo-constants';


const Show_Report = (props) => {
  console.log('=== props ===>',props)
   const [complaint, setComplaint]=useState(null)
   const [bias, setBias]=useState(null)
   const [agressor, setAgressor]=useState(null)
   const [acting, setActing]=useState(null)
   const [info, setInfo]=useState(null)

  const handleUpdate = async () => {
    let url =`http://157.245.65.53/server/report/update`  
    try{
      const data = {
               createdAt:createdAt|| props.old_report.createdAt,
               complaint : complaint || props.old_report.complaint, 
               bias:bias || props.old_report.bias,  
               agressor:agressor || props.old_report.agressor,  
               acting:acting || props.old_report.acting,  
               info:info || props.old_report.info,
               record_Location:props.old_report.record_Location,
               proof: props.old_report.proof,
               loc_victim: props.old_report.loc_victim
                  }
                  console.log('===================>',data)
    const res = await axios.post(url,data);
     console.log(res)
    props.getUserReports()
      Alert.alert('Your report has been updated')
    }catch(error){
      Alert.alert('Something went wrong')
    }
  }
return (
<View style={styles.container}>
     <View style={styles.statusBar} />
    <View style={styles.header}>
            <Image
            style={{ width: 120, height: 120, }} 
            source={require('../assets/images/logo_white.png')}
            />   
    </View>

    <View style={styles.main}>
      <Text style=
            {{color: 'white',  
              fontSize:20, 
              fontWeight:'bold',
              marginBottom:60}}> MY REPORT </Text>  
     <ScrollView>

      <View style={styles.records}>
                 <Text style= {{color: 'white',  
                                fontSize:15, 
                                fontWeight:'bold',
                                marginLeft:10}}>Made on:</Text>

                      <Text style= {{color: 'darkorange',  
                                 fontSize:15, 
                                 marginRight:10}}>{props.old_report.createdAt}
                                 </Text> 
                                             
      </View>

       
      <View style={styles.records}>
                 <Text style= {{color: 'white',  
                                fontSize:15, 
                                fontWeight:'bold',
                                marginLeft:10}}>Complaint:</Text>

                      <Text style= {{color: 'darkorange',  
                                 fontSize:15, 
                                 marginRight:10}}>{props.old_report.complaint}
                                 </Text> 
                                             
      </View>
      
      <View style={styles.records}>
                 <Text style= {{color: 'white',  
                                fontSize:15, 
                                fontWeight:'bold',
                                marginLeft:10}}>On Grounds of:</Text>
                      <Text style= {{color: 'darkorange',  
                                 fontSize:15, 
                                 marginRight:10}}>{props.old_report.bias}
                                 </Text>                      
      </View>
      
      <View style={styles.records}>          
                <Text style= {{color: 'white',  
                               fontSize:15, 
                               fontWeight:'bold',
                               marginLeft:10 }}>Perpetrated by:</Text>

                      <Text style= {{color: 'darkorange',  
                                 fontSize:15, 
                                 marginRight:10}}>{props.old_report.agressor}
                                 </Text>                      

                     
      </View>
      
      <View style={styles.records}> 
                <Text style= {{color: 'white',  
                               fontSize:15, 
                               fontWeight:'bold',
                               marginLeft:10}}>Acting:</Text>

                      <Text style= {{color: 'darkorange',  
                                 fontSize:15, 
                                 marginRight:10}}>{props.old_report.acting}
                                 </Text>                      
                     
      </View>
      

      <View style={styles.records}>          
                <Text style= {{color: 'white',  
                               fontSize:15, 
                               fontWeight:'bold',
                               marginLeft:10}}>What happened:</Text>
                     
                      <Text style= {{color: 'darkorange',  
                                 fontSize:15, 
                                 marginRight:10}}>{props.old_report.info}
                                 </Text>                      
                     
      </View>


      <View style={styles.records}>          
                <Text style= {{color: 'white',  
                               fontSize:15, 
                               fontWeight:'bold',
                               marginLeft:10}}>Where happened:</Text>
                     
                      <Text style= {{color: 'darkorange',  
                                 fontSize:15, 
                                 marginRight:10}}>{props.old_report.record_Location}
                                 </Text>                      
                     
      </View>

                    
     </ScrollView>
    </View>


</View>

    )
}

export default Show_Report;

const styles = StyleSheet.create({
  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  

  input:{
    height: 40, 
    width:200,
    marginTop:4,
    backgroundColor: 'black',
    borderBottomColor: 'gray',
    borderWidth: 2,
    color: 'white'
  },


  records:{
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection:'row',
    backgroundColor:'black',
    borderColor:'white',
    color:'white',
    borderWidth:1,
    borderRadius:5,
    width:370,
    height: 50,
    
  },

  fileicons:{
   alignItems: 'center',
   justifyContent: 'space-around',
   flexDirection:'row',
  },
});

//  <Button 
             //   title='Update it!'
             //   color='darkorange'
             //   onPress= {()=>handleShare()}
             // />   
             