import React , {useState} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         Alert,
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image,
         TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faSearchLocation } from '@fortawesome/free-solid-svg-icons';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';

const Tabbar = (props) => { 


// const [buttonColor,setButtonColor] = useState('')
// onButtonPress = () => {  
//   setButtonColor({ buttonColor: 'darkorange' }); 
// }


 return <View style={styles.navbar}>

          <TouchableOpacity style={styles.navicon}
                            onPress= {()=>props.changePage('home')}
                            >
            <FontAwesomeIcon icon={faHome} color={ 'black' } size={25}/> 
          </TouchableOpacity>
          
          <TouchableOpacity style={styles.navicon}
                            onPress= {()=>props.changePage('files')}>
            <FontAwesomeIcon icon={faUserCircle} color={ 'black' } size={25}/> 
          </TouchableOpacity>
           
          <TouchableOpacity style={styles.navicon}
                            onPress= {()=>props.changePage('report')}>
            <FontAwesomeIcon icon={faMapMarkerAlt} 
                             color={ 'darkorange' } 
                             size={30} 
                             style={{marginBottom:10}}
                             />                    
           

          </TouchableOpacity>
          
          <TouchableOpacity style={styles.navicon}
                            onPress= {()=>props.changePage('map')}>
            <FontAwesomeIcon icon={faSearchLocation} color={ 'black' } size={25}/> 
          </TouchableOpacity>

          <TouchableOpacity style={styles.navicon}>
            <FontAwesomeIcon icon={faSignOutAlt} color={ 'black' } size={25}
                             onPress={()=>Alert.alert(
                                   'Sign Out',

                                   'Do you want to proceed?',
                                  [
                                  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                  {text: 'OK', onPress: () => props.getOut()},
                                  ],
                                  { cancelable: false }
                                   )
                                  }    /> 

             
          </TouchableOpacity>
  </View>
 }  

export default Tabbar;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:'100%',
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  
  // navwrapper:{
  //   flexDirection:'row',
  //   width:'100%',
  //   height:'10%', 
  // },
   
  navbar:{
    alignItems: 'flex-start',
    paddingVertical: 20,
    flexDirection:'row',
    width:'100%',
    height:'8%',
    backgroundColor:'white',
    justifyContent: 'space-around',
   
  },    

  navicon:{
   alignItems: 'center',
   justifyContent: 'space-around',

  },

  // icon:{
  //  alignItems: 'center',
  //  justifyContent: 'space-between',
  //  paddingHorizontal:10
  // },

  records:{
    alignItems: 'flex-start',
    paddingVertical: 100,
    flexDirection:'row',
    width:'100%',
    height:'10%',
    left: 30,
    
  },


});

 // elevation:4,
    // shadowOffset: { width: 5, height: 5 },
    // shadowColor: "grey",
    // shadowOpacity: 0.5,
    // shadowRadius: 10,

//<Text style={{paddingVertical: 20, marginTop:4}}>Report</Text>}*//