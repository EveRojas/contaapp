import React , {useState} from 'react';
import axios from 'axios';
import {
  View,
  AppRegistry,
  Platform,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Marker from 'react-native-maps';

const {width, height}= Dimensions.get('window')

const SCREEN_HEIGHT= height
const SCREEN_WIDHT= width
const ASPECT_RATIO= width/height
const LATIDUDE_DELTA=0.0022
const LONGITUDE_DELTA= LATIDUDE_DELTA + ASPECT_RATIO

export default class MapIntro extends React.Component {
 
constructor(props){
 super(props)
 this.state = {
     initialPosition:{
      latitude:0,
      longitude: 0,
      latitudeDelta:0,
      longitudeDelta:0
     },
     markerPosition:{
       latitude:0,
       longitude:0,
       latitudeDelta:0,
       longitudeDelta:0
     }
     
     }
   };

watchID:?number= null

componentDidMount() {

 navigator.geolocation.getCurrentPosition((position) => {
        var lat = parseFloat(position.coords.latitude)
        var long = parseFloat(position.coords.longitude)
       
        var initialRegion ={
          latidude:lat,
          longitude:long,
          longitudeDelta:LATIDUDE_DELTA,
          longitudeDelta:LONGITUDE_DELTA
         
             }

          this.setState({initialPosition:initialRegion})
          this.setState({markerPosition: initialRegion})
         },
         {enableHightAccuracy:true, timeout:100, maxminAge:1000}
        );

    this.watchID=navigator.geolocation.watchPosition((position)=>{
      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)

      var lastRegion={
        latitude:lat,
        longitude:long,
        latitudeDelta: LATIDUDE_DELTA,
        longitudeDelta:LONGITUDE_DELTA
      }

      this.setState({initialPosition:lastRegion})
      this.setState({markerPosition:lastRegion})
    })
}
      componentWillMount (){
      navigator.geolocation.clearWatch(this.watchID)
    }

render(){

  const mapStyle= [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]


return (

    <View style={styles.main}>
        <MapView
              style={styles.map}
              provider={PROVIDER_GOOGLE}
              region={this.state.initialPosition}
              customMapStyle={mapStyle}
              showsUserLocation={false}
              followUserLocation={false}
              >
          <MapView.Marker image={require('../assets/images/walkpin100.png')}
                          description={"Don't keep intolerance invisible, report it!"}
                          coordinate={this.state.markerPosition}
                                       >
          </MapView.Marker>
          
        </MapView>
        
    </View>
  

    )
  }
}


const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:380,
    height:'12%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'80%',
   
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  
  navbar:{
    alignItems: 'flex-start',
    paddingVertical: 15,
    flexDirection:'row',
    width:'100%',
    height:'10%',
    borderWidth: 1,
    backgroundColor:'white',
    borderColor:'black',
    justifyContent: 'space-around',

  },    
  
  map:{
    height:480, 
    width:380,
    marginTop:0,
    marginBottom:0,
    borderTopColor:'gray',
    borderWidth:1,
    borderBottomColor:'gray',
   
  },

 search:{
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection:'row',
    backgroundColor:'black',
    borderTopColor:'white',
    borderBottomColor:'white',
    //borderColor:'white',
    borderWidth:1,
    //borderRadius:5,
    width:380,
    height: 50,
    
  },

});

