import React , {useState, useEffect} from 'react';
import axios from 'axios';
import { StyleSheet, 
         Text, 
         View, 
         TextInput,
         Button, 
         ScrollView,
         AsyncStorage,
         Modal,
         Picker,
         Image } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Login from './components/login';
import Register from './components/register';
import Files from './components/files';
import Tabbar from './components/tabbar';
import MapSearch from './components/map';
import MapIntro from './components/mapintro';
import Numbers from './components/numbers';
import Report from './components/report';
import Home from './components/home';
import EditReport from './components/editreport';
import Show_Report from './components/show_report';
import Coords from './country_coords';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';



const App = () => {  
  const [loc_victim, setLoc_victim]= useState({})
  const [stats, setStats]= useState([])
  const [numbers, setNumbers]=useState([])
  const [user_id, setUser_id]=useState('')
  const [old_report, set_old_report]=useState({})
  const [user_reports, setUser_reports]=useState([])
 
  useEffect(()=>{
    getLocation()
    getLogged()
    getReports()
  //getUserReports()
  //getNumbers()
  //getLocationAsync()
  },[])

// async function getLocationAsync() {
//   console.log('--------')
//   // permissions returns only for location permissions on iOS and under certain conditions, see Permissions.LOCATION
//   const { status, permissions } = await Permissions.askAsync(Permissions.LOCATION);
//   if (status === 'granted') {
//     return Location.getCurrentPositionAsync({ enableHighAccuracy: true });
//   } else {
//     throw new Error('Location permission not granted');
//   }
// }

  getReports = async () => {
      let url =`http://157.245.65.53/server/report/getall`;
      try {
        const res = await axios.get(url);
        const temp_stats=[]
        const reports = res.data
        //console.log(Coords)
        ///console.log( 'APP REP==>',res.data)
        res.data.forEach( (ele,i) => {
          const rec_loc_idx = Coords.findIndex( loc => ele.record_Location === loc.name)
          const stats_idx = temp_stats.findIndex( loc2 => loc2.name === ele.record_Location)
          //console.log('rec_loc_idx =',rec_loc_idx)
           //console.log('TEMP STATS 0 LEN ===>', temp_stats.length)
           //console.log('TEMP STATS 0 ===>',  temp_stats)
          if( stats_idx === -1 ){
              console.log('ele stats ===>',ele)
              //console.log('ele.bias ===>',ele.bias)
              temp_stats.push({
                name: ele.record_Location,
                latd: Coords[rec_loc_idx].latd,
                long: Coords[rec_loc_idx].long,
                count: 1,
                complaint:{[ele.complaint]:1},
                bias:{[ele.bias]:1},
                _id: ele._id
              })
          }else{
              
            if(ele.complaint in temp_stats[stats_idx].complaint){
              temp_stats[stats_idx].complaint[ele.complaint] = temp_stats[stats_idx].complaint[ele.complaint] + 1
            } else {
              temp_stats[stats_idx].complaint[ele.complaint] = 1
            // console.log('ele comp =>',ele.complaint)
            }
               
            if(ele.bias in temp_stats[stats_idx].bias){
              temp_stats[stats_idx].bias[ele.bias] = temp_stats[stats_idx].bias[ele.bias] + 1
            } else {
              temp_stats[stats_idx].bias[ele.bias] = 1
            // console.log('ele bias =>',ele.bias) 
            }

            temp_stats[stats_idx].count = temp_stats[stats_idx].count + 1
           // console.log('TEMP',temp_stats)

        }
        //console.log('TEMP',temp_stats)
         
        })
        console.log('TEMP last',temp_stats) 
         setStats(temp_stats)   
           

          } catch (error) {
        console.log(error)      
      }
    }



  getLocation=()=>{
    navigator.geolocation.getCurrentPosition(position => {
      //console.log('=== coords ===>',position.coords)
      const coords = {
            lat:position.coords.latitude,
            lng:position.coords.longitude
          }
       setLoc_victim(coords)
       
    })

  }
     
   const [page,setPage] = useState('login')
   const [isLogged, setIsLogged] = useState(false)
   //use useEffect to check if initially there is a token in AsyncStorage and verify it in the server
  getLogged = async () =>{ 
   try{
       //await AsyncStorage.removeItem('token')
      const token = await AsyncStorage.getItem('token') 
      //console.log('===  TOKEN  ===>', token)
      if( token !== null ){
         const response = await axios.post('http://157.245.65.53/server/users/verify_token',{token:JSON.parse(token)})
         //console.log('===  RESPONSE  ===>', response)
         response.data.ok 
         ?( setIsLogged(true) , setUser_id(response.data.user_id) , setPage('home') )
         : getOut()   
      }  
  }
   catch(error){
          console.log(error)
        }
  }
        
  getOut = async () =>{ 
   try{

      const token = await AsyncStorage.removeItem('token')
      setUser_id('') 
      setIsLogged(false)
      setPage('login')
  }
   catch(error){
          console.log(error)
        }
  }
 
  const getNumbers = async (num) => {
    console.log('NUMBER', num)
    await setNumbers(num)
    await changePage('numbers')
  }


  const changePage = async (page,report) => {
         try{
           if(report){
              await set_old_report(report)
              setPage(page)
           } 
           setPage(page)
         }catch(error){

         }
         
   }

  const changeLogStatus = () => { setIsLogged(true) ; setPage('home') }
   
   const getUserReports = async () => {
          let url =`http://157.245.65.53/server/report/user_reports`;
         try {
          const token = await AsyncStorage.getItem('token')
          //console.log( 'TOKEN==>',token)
          const temp = JSON.parse(token) 
          const res = await axios.post(url,{token:temp})
          console.log('res post =>',res)
         if(res.data.ok){
            //AsyncStorage.setItem('token',JSON.stringify(response.data.token))
            setUser_reports([...res.data.user_reports])
           console.log( 'USER REPS==>',temp)
         }
        //setUser_id()
 
      } catch (err) {
        alert('something went wrong')
      }
  }
   deleteUserReport = async (i) => {
    let url =`http://157.245.65.53/server/report/remove`;
    try{  
     
       const res = await axios.post(url,{_id:user_reports[i]._id})
       console.log(res)
       if (res.data.ok){ 
       var update_userep= user_reports
       update_userep.splice(i,1); 
       await AsyncStorage.setItem('user_reports', JSON.stringify(update_userep));
       setUser_reports([...update_userep])
       }
      } catch (err) {
        alert('something went wrong')
      }
  }
  const renderPages = () => {
         if(isLogged){
            if(page === 'home')return <Home changePage = {changePage}/>
            if(page === 'files')return <Files old_report={old_report} 
                                              changePage = {changePage}
                                              getUserReports={getUserReports}
                                              user_reports={user_reports}
                                              deleteUserReport={deleteUserReport}/>          
            if(page === 'report')return <Report loc_victim = {loc_victim} 
                                                user_id={user_id} 
                                                changePage = {changePage}/>
            if(page === 'map')return <MapSearch loc_victim = {loc_victim}
                                                changePage = {changePage} 
                                                      stats= {stats} 
                                                 getNumbers= {getNumbers}/>
            if(page === 'numbers')return <Numbers  stats={stats}  numbers={numbers} changePage = {changePage}/>
            if(page === 'mapintro')return <MapIntro changePage = {changePage}/> 
            if(page === 'editreport')return <EditReport old_report={old_report} 
                                                        changePage = {changePage}
                                                        getUserReports={getUserReports}/>
            if(page === 'show_report')return <Show_Report old_report={old_report} 
                                                          changePage = {changePage}
                                                          getUserReports={getUserReports}/>  
         }else{
            if(page === 'login')return <Login changeLogStatus = {changeLogStatus} changePage = {changePage}/>
            if(page === 'register')return <Register changePage = {changePage}/>

         }
   }

   return <View style={styles.container}>
             {
              renderPages()
             }
 
             {
              isLogged ? <Tabbar  changePage = {changePage} getOut={getOut}/> : null 
             }
          </View>

 }

export default App;

const styles = StyleSheet.create({
  
  statusBar: {
    height: Constants.statusBarHeight,
  },

  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: 'center',
    justifyContent: 'center',
  },

  header:{
    width:'100%',
    height:'15%',
    marginTop: 5,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  main:{
    width:'100%',
    height:'70%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor:'black',
    color:'white',
    alignItems: 'center',

  },
  
  login:{
   
   width:'30%',
   marginTop:100,
   alignItems: 'center',
   justifyContent: 'space-between',

  },

  input:{
    height: 40, 
    width:200,
    marginTop:4,
    backgroundColor: 'black',
    borderBottomColor: 'gray',
    borderWidth: 2,
    color: 'white'
  },

  footer:{
    width:'100%',
    height:'10%',
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor:'black',
    borderColor:'black',
    alignItems: 'center',
    justifyContent: 'space-between',

  },    


});
